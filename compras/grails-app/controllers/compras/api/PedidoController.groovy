package compras.api
import grails.plugins.rest.client.RestBuilder

class PedidoController {

    static scaffold = Pedido

    def compraService

    def criar(){
    	if (!session.user) {
    		render(status:500, text:"Você precisa estar logado para realizar uma compra")
    		
    	}else{

		def list = request.JSON
		def restBuilder = new RestBuilder()
	    def response = restBuilder.post("http://localhost:8080/pedido"){
	        contentType "application/json"
	        json {list : list}

		}
		def resumo = response.json

		compraService.registrar(resumo, session.user);


	    return "index.html"  
}
    }
}

