package compras.api

class UsuarioController {

    static scaffold = Usuario

    def usuarioService
       def springSecurityService


    def login(){
		def list = request.JSON
    	def usuario = usuarioService.login(list)
    	if (usuario) {
			 def u =['email' : usuario.email,
		             'senha' : usuario.senha
		            ] 
		    session.user = u	
    	}

    	return "index.html"
    }

}
