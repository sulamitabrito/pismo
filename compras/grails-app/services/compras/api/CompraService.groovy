package compras.api

import grails.transaction.Transactional

@Transactional
class CompraService {

    def registrar(resumoPedido, user) {
	
	def compra = new Compra()
	compra.usuario = user.email
	compra.valorCobrado = resumoPedido.valorCobrado
	compra.valorDesconto = resumoPedido.valorDesconto
	compra.valorSemDesconto= resumoPedido.valorTotal
	compra.save(flush:true)

println compra

    }
}
