package compras.api

class Pedido {
	String produto;
	Integer quantidade;


    static constraints = {
		produto nullable:false
		quantidade nullable:false
    }

}
