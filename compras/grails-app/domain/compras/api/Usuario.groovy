package compras.api

class Usuario {

	String nome;
	String email;
	//Sem criptografia para testes
	String senha;
	boolean newsletter;

    static constraints = {
    	nome nullable:false
    	email nullable:false
    	senha nullable : false
    	newsletter defaultValue:true
    }
}
