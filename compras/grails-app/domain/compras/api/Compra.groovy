package compras.api

class Compra {

	Double valorCobrado;
	Double valorDesconto;
	String usuario;
	Double valorSemDesconto;

    static constraints = {
    	usuario nullable:false
    }
}
