Para esse sistema foi utilizado o framework grails que traz uma plataforma mais simples e enxuta para a programação de sistemas Groovy e o banco de dados mySql que é de facil utilização e é suportado pela maioria dos servidores.

Sistema compras-api

Para realizar uma compra é necessário estar logado.
Para o login enviar nome e senha na url login exemplo:

pismo.underjelastic.com.br/compras-api/login

É possivel cadastrar usuarios usando a url abaixo:
pismo.underjelastic.com.br/compras-api/usuario/create

[
    {"nome":"Teste", "senha":"senha"}
]

Para realizar pedidos basta enviar o nome do produto e a quantidade.
Exemplo:

pismo.underjelastic.com.br/compras-api/pedido

[
{"produto":"Camiseta", "quantidade":1}]

Sistema produtos-api
É possivel cadastrar produtos pela url abaixo:

pismo.underjelastic.com.br/produtos-api/produto
{     
  "nome" : "camiseta",
  "quantidade" : 10,
  "valor" : 30.5
}

É possivel dar  de desconto para produtos cadastrados, passando a porcentagem do desconto e o id do produto.
Exemplo:

pismo.underjelastic.com.br/produtos-api/desconto
{    
  "desconto" : 50,
"id" : 1 }