package produtos.api

import grails.transaction.Transactional
import grails.converters.*


@Transactional
class ProdutoService {

	//O sistema não está preparado para verificar quantidades passadas em mais de uma linha no json

	def verEstoque(verificar){
		def estoque = Produto.findByNome(verificar.produto)	
		println estoque.quantidade
		println verificar.quantidade
		if (estoque.quantidade >= verificar.quantidade ) {
			return estoque
		}			
		else 
		 return null			
	}		

	def comprar(list){
		def valorTotal =0
		def valorDesconto = 0
		def valorCobrado =0
		list.each{
			def produto = verEstoque(it)
			if (produto) {
				def valorParcial =  produto.valor * it.quantidade
				valorTotal += valorParcial

				if (produto.desconto !=0) {
					def desconto = valorParcial *(produto.desconto/100)
					valorDesconto += desconto
				}


			}
			

		}
		if (valorTotal!=0) {
			valorCobrado = valorTotal - valorDesconto

			def retorno =  ["valorTotal" : valorTotal,
							"valorDesconto" : valorDesconto,
							"valorCobrado" : valorCobrado]
			def jsonRetorno = (retorno as JSON)

			return jsonRetorno

		}
		else{
			def retorno = [status : 500,
							 text : "Não há quantidade suficiente para a compra desse produto"]
			return retorno
	}
	}
	
	
		
	


    
}
