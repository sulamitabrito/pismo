package produtos.api

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }

        }
        "/validar"(controller:"estoque", action:"index")
 
        "/desconto"(controller:"produto", action:"setDesconto")

        "/quantidade"(controller:"produto", action:"buscaQuantidadeProduto")

        "/pedido"(controller : "produto", action: "pedido")


        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
