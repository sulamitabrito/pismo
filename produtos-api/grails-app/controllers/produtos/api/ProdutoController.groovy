package produtos.api
import grails.converters.*
import groovy.json.JsonSlurper


class ProdutoController {

    static scaffold = Produto

    def produtoService

    //inserir desconto para um produto
	def setDesconto(){

	    	def porcentagem = request.JSON.desconto
	    	def id = request.JSON.id

	    	if (porcentagem > 100 || porcentagem < 0) {
				render(status : 500, text : "Porcentagem para desconto inválido")
	    	}
	    	else{
		    	def produto = Produto.findById(id)
		    	produto.desconto = porcentagem
		    	produto.save(flush:true)
				return "index.html";
		}
			
	}

	def buscaQuantidadeProduto(){
		def nome = request.JSON.nome
		println nome
		produtoService.verEstoque(nome);
		return "index.html";
	}	


	def pedido(){
		def list = request.JSON
		println request
		def jsonString = (list as JSON).toString()
	
		render produtoService.comprar(list);
	 

	}
}

