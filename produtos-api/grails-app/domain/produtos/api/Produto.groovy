package produtos.api

class Produto {

	String nome;
	Integer quantidade;
	Double valor;
	Double desconto;


    static constraints = {
	    nome nullable:false, blank:false
	    quantidade nullable:false, blank:false
	    valor nullable:false, blank:false
	    desconto nullable:false, blank:false
    }
}
